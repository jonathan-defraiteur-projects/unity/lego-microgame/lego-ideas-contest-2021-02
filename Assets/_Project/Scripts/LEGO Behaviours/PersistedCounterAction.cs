using Unity.LEGO.Behaviours.Actions;
using Unity.LEGO.Game;
using UnityEngine;

namespace JonathanDefraiteur.LEGO.Behaviours.Actions
{
    public class PersistedCounterAction : RepeatableAction
    {
        [SerializeField, Tooltip("The variable to modify.")]
        Variable m_Variable = null;

        [SerializeField, Tooltip("The operator to apply between value and variable.")]
        CounterAction.Operator m_Operator = CounterAction.Operator.Add;

        [SerializeField, Tooltip("The value to use with the operator to modify variable.")]
        int m_Value = 1;

        float m_Time;
        bool m_HasUpdatedVariable;

        protected override void Reset()
        {
            base.Reset();

            m_Repeat = false;
            m_IconPath = "Assets/_Project/Gizmos/LEGO Behaviour Icons/Persisted Counter Action.png";
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            m_Pause = Mathf.Max(m_Pause, 0.0f);
        }

        protected override void Start()
        {
            base.Start();

            VariableManager.RegisterVariable(m_Variable);
            if (PlayerPrefs.HasKey(m_Variable.name)) {
                VariableManager.SetValue(m_Variable, PlayerPrefs.GetInt(m_Variable.name));
            }
        }

        protected void LateUpdate()
        {
            if (m_Active && null != m_Variable)
            {
                m_Time += Time.deltaTime;

                if (!m_HasUpdatedVariable)
                {
                    UpdateVariable();
                    m_HasUpdatedVariable = true;
                }

                if (m_Time >= m_Pause)
                {
                    m_Time -= m_Pause;
                    m_HasUpdatedVariable = false;
                    m_Active = m_Repeat;
                }
            }
        }

        protected void UpdateVariable()
        {
            switch (m_Operator)
            {
                case CounterAction.Operator.Add:
                    {
                        SetNewValue(VariableManager.GetValue(m_Variable) + m_Value);
                        break;
                    }
                case CounterAction.Operator.Subtract:
                    {
                        SetNewValue(VariableManager.GetValue(m_Variable) - m_Value);
                        break;
                    }
                case CounterAction.Operator.Multiply:
                    {
                        SetNewValue(VariableManager.GetValue(m_Variable) * m_Value);
                        break;
                    }
                case CounterAction.Operator.Set:
                    {
                        SetNewValue(m_Value);
                        break;
                    }
            }
        }

        protected void SetNewValue(int _value)
        {
            VariableManager.SetValue(m_Variable, _value);
            PlayerPrefs.SetInt(m_Variable.name, _value);
            PlayerPrefs.Save();
        }
    }
}