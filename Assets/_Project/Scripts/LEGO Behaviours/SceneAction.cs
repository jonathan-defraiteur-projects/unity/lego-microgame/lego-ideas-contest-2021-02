﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Action = Unity.LEGO.Behaviours.Actions.Action;

namespace JonathanDefraiteur.LEGO.Behaviours.Actions
{
    public class SceneAction : Action
    {
        [SerializeField, Tooltip("The name of the scene you want to load when the action is triggered.")]
        string m_Scene = "";
        
        bool m_IsLoading;
        
        protected override void Reset()
        {
            base.Reset();

            m_Scene = "";
            m_IconPath = "Assets/_Project/Gizmos/LEGO Behaviour Icons/Scene Action.png";
        }

        private void OnEnable()
        {
            if (m_Scene == "") {
                enabled = false;
            }
        }

        protected void Update()
        {
            if (!m_Active || m_IsLoading)
                return;

            // TODO: Manage a better GameFlowManager
            // GameFlowManager.PreviousScene = SceneManager.GetActiveScene().name;
            SceneManager.LoadSceneAsync(m_Scene);
            m_IsLoading = true;
        }
    }
}
