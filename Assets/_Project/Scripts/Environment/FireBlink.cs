﻿using UnityEngine;

public class FireBlink : MonoBehaviour
{
    private Light _light;
    private float _offset = 0;
    [SerializeField] protected float min = 0;
    [SerializeField] protected float max = 1;

    void Awake()
    {
        _offset = Random.value * 100;
        if (!TryGetComponent(out _light)) {
            enabled = false;
        }
    }

    void Update()
    {
        _light.intensity = Mathf.Lerp(min, max,(Mathf.Sin(Time.time * 2) + Mathf.Sin((Time.time + _offset) * 10)) / 2);
    }
}
