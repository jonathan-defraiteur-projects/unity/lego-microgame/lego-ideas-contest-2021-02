﻿using JonathanDefraiteur.LEGO.Behaviours.Actions;
using UnityEditor;
using Unity.LEGO.EditorExt;

namespace JonathanDefraiteur.LEGO.EditorExt
{
    [CustomEditor(typeof(SceneAction), true)]
    public class SceneActionEditor : ActionEditor
    {

        SerializedProperty m_SceneProp;
        
        protected override void OnEnable()
        {
            base.OnEnable();

            m_SceneProp = serializedObject.FindProperty("m_Scene");

        }

        protected override void CreateGUI()
        {
            EditorGUI.BeginDisabledGroup(EditorApplication.isPlaying);

            EditorGUILayout.PropertyField(m_SceneProp);

            EditorGUI.EndDisabledGroup();
        }
    }
}